export class CategoryModel {
    name: string;
    sort_name: string;
    id: number;
    shortname: string;
    groups?: any[] = [];

    constructor() {
        this.name = null;
        this.sort_name = null;
        this.id = null;
        this.shortname = null;
        this.groups = [];
    }
}
