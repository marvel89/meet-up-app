export interface ICategories {
    name: string;
    sort_name: string;
    id: number;
    shortname: string;
    groups?: any[];
}
