import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

    constructor() {}

    onDeactivate(event) { window.scrollTo(0, 0); }

    onActivate(event) { window.scrollTo(0, 0); }
}
