import { Injectable } from '@angular/core';

import { STORAGE_VARIABLES_EXPORT } from '../../shared/constants/storage.contants';

@Injectable()
export class StorageService {

    constructor() { }

//  ** Local Storage **/
    getLocalStorageItem( key: string , default_value?: string): any {
        return localStorage.getItem( key) || default_value;
    }

    setLocalStorageItem( key: string , value: string) {
        localStorage.setItem( key, value);
    }

    getLocalStorageObject( key: string , default_value?: string): any {
        return JSON.parse( localStorage.getItem(key) ) || default_value;
    }

    setLocalStorageObject( key: string , value: any) {
        localStorage.setItem( key, JSON.stringify(value));
    }

    deleteLocalStorage( key: string ) {
        localStorage.removeItem( key );
    }

    deleteAllLocalStorage() {
        localStorage.clear();
    }

//  ** Session Storage **/
    setSessionStorageItem( key: string , value: string) {
        sessionStorage.setItem( key, value );
    }

    getSessionStorageItem( value: string, default_value?: string ): any {
        return sessionStorage.getItem(value) || default_value;
    }

    setSessionStorageObject( key: string , value: any) {
        sessionStorage.setItem( key, JSON.stringify( value ));
    }

    getSessionStorageObject( key: string , default_value?: string): any {
        return JSON.parse( sessionStorage.getItem( key ) ) || default_value;
    }

    deleteSessionStorage( key: string ) {
        sessionStorage.removeItem( key );
    }

    deleteAllSessionStorage() {
        sessionStorage.clear();
    }
}

export const STORAGE_VARIABLES = STORAGE_VARIABLES_EXPORT;
