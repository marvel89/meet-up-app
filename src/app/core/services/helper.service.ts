import { Injectable } from '@angular/core';
import { ICategories } from '../../shared/interfaces/category.interface';
import { STORAGE_VARIABLES, StorageService } from './storage.service';

@Injectable()
export class HelperService {

    constructor(private storage_service: StorageService) { }

    removedPreferredCatOnCatList(categories): ICategories[] {
        const preferred_category: ICategories = this.storage_service.getLocalStorageObject(STORAGE_VARIABLES.PREFERRED_CATEGORY);
        if (preferred_category.id && categories) {
            const categories_arr = [];
            categories.forEach( (cat: ICategories) => {
                if (cat.id !== preferred_category.id) {
                    categories_arr.push(cat);
                }
            });
            return categories_arr;
        } else {
            return categories;
        }
    }

    formatCategoriesForAPIParam(): string {
        const preferred_category: ICategories = this.storage_service.getLocalStorageObject(STORAGE_VARIABLES.PREFERRED_CATEGORY);
        const selected_catalogues: ICategories[] = this.storage_service.getSessionStorageObject(STORAGE_VARIABLES.OTHER_CATEGORIES);
        let categories_params: string = '';
        if (preferred_category.id) {
            categories_params = preferred_category.id.toString();
            if (selected_catalogues && selected_catalogues.length > 0) {
                selected_catalogues.forEach((selected_category) => {
                    categories_params += `,${selected_category.id}`;
                });
            }
        }
        return categories_params;
    }
}
