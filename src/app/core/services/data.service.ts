import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParameterCodec, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { API_END_POINT } from '../../shared/constants/api.constants';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';


@Injectable()
export class DataService {
    baseUrl: string = 'https://cors-anywhere.herokuapp.com/https://api.meetup.com/';
    // Using Heroku app proxy to bypass cors rules
    // Ref: (https://stackoverflow.com/questions/44448177/no-access-control-allow-origin-angular-4-app)

    constructor(private http:  HttpClient) { }

    dataGet(route?: string , params?: HttpParams): Observable< any > {
        if (!params) { params = new HttpParams({ encoder: new CustomEncoder() }); }
        params = params.append('key', environment.meet_up_api_key);
        return this.http.get(this.baseUrl + route, { params: params, headers: this.setGetHeader() })
        .map((res: any) => {
            return res;
        })
        .catch(this.handleError);
    }

    dataPost(route?: string , body?: HttpParams): Observable< any > {
        if (!body) { body = new HttpParams({ encoder: new CustomEncoder() }); }
        body = body.append('key', environment.meet_up_api_key);
        return this.http.post(this.baseUrl + route,  body , { headers: this.setPostHeader() } )
        .map((res: any) => {
            return res;
        })
        .catch(this.handleError);
    }

    setGetHeader() {
        return new HttpHeaders({ 'Accept': 'application/json', });
    }

    setPostHeader() {
        return new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        });
    }

    private handleError(error: any) {
        console.error('server error:', error);
        if (error instanceof Response) {
            let errMessage: any;
            try {
                errMessage = error.json().catch( (err: any) => err);
            } catch (err) {
                errMessage = error.statusText;
            }
            return Observable.throw(errMessage);
        }
        return Observable.throw(error || 'server error');
    }
}


export const API_END_POINTS = API_END_POINT;

// Angular convert any + to space, this encode would convert to ASCII Space
// Reference https://github.com/angular/angular/issues/18261 :)
export class CustomEncoder implements HttpParameterCodec {
    encodeKey(key: string): string {
      return encodeURIComponent(key);
    }
    encodeValue(value: string): string {
      return encodeURIComponent(value);
    }
    decodeKey(key: string): string {
      return decodeURIComponent(key);
    }
    decodeValue(value: string): string {
      return decodeURIComponent(value);
    }
}
