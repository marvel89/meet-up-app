import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { OtherCategoriesComponent } from '../../other-categories/other-categories.component';
import { ICategories } from '../../../shared/interfaces/category.interface';
import { STORAGE_VARIABLES, StorageService } from '../../../core/services/storage.service';

@Component({
  selector: 'app-change-preferred-category-modal',
  templateUrl: './change-preferred-category-modal.component.html',
  styleUrls: ['./change-preferred-category-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChangePreferredCategoryModalComponent implements OnInit {
    selected_category: ICategories;

    constructor (
        public dialogRef: MatDialogRef<OtherCategoriesComponent>,
        private storage_service: StorageService,
        @Inject(MAT_DIALOG_DATA) public categories?: ICategories[]) {}

    ngOnInit() { }

    closeModal(): void {
       this.dialogRef.close({ action: 'closed', data: null });
    }

    savePreferredCategory(): void {
        if (this.selected_category) {
            this.storage_service.setLocalStorageObject(STORAGE_VARIABLES.PREFERRED_CATEGORY, this.selected_category);
        }
        this.dialogRef.close({ action: 'updated', data: this.selected_category });
    }
}
