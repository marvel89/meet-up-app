import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { ChangePreferredCategoryModalComponent } from '../modals/change-preferred-category-modal/change-preferred-category-modal.component';
import { ICategories } from '../../shared/interfaces/category.interface';
import { API_END_POINTS, DataService} from '../../core/services/data.service';
import { StorageService, STORAGE_VARIABLES } from '../../core/services/storage.service';
import { HelperService } from '../../core/services/helper.service';

@Component({
  selector: 'app-other-categories',
  templateUrl: './other-categories.component.html',
  styleUrls: ['./other-categories.component.scss']
})
export class OtherCategoriesComponent implements OnInit, OnDestroy {
  is_loading: boolean = true;
  all_catalogues: ICategories[];
  other_catalogues: ICategories[];
  selected_catalogues: ICategories[] = [];
  preferred_category: ICategories;
  dialog_subscription: Subscription;

  constructor(private data_service: DataService,
              private snackBar: MatSnackBar,
              private storage_service: StorageService,
              private helper_service: HelperService,
              private _route: Router,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.getPreferredCategory();
    this.fetchCategories();
  }

  ngOnDestroy() {
    if (this.dialog_subscription) { this.dialog_subscription.unsubscribe(); }
  }

  getPreferredCategory(): void {
      this.preferred_category = this.storage_service.getLocalStorageObject(STORAGE_VARIABLES.PREFERRED_CATEGORY);
  }

  fetchCategories(): void  {
    this.data_service.dataGet(API_END_POINTS.GET_CATEGORIES)
    .subscribe((response: any) => {
        this.is_loading = false;
        if (response) {
            this.all_catalogues = response.results;
            this.other_catalogues = this.helper_service.removedPreferredCatOnCatList(this.all_catalogues);
        }
    },
    (err: any) => {
        console.log(err);
        this.snackBar.open('An error happened while connecting to the api', 'Ok', { duration: 5000 });
    });
  }

  catalogueChanged(catalogue: ICategories, value): void  {
      if (value === true) {
          this.selected_catalogues.push(catalogue);
      } else {
        for (let i = 0; i < this.selected_catalogues.length; i++) {
            if (this.selected_catalogues[i].id === catalogue.id) {
                this.selected_catalogues.splice(i, 1);
                break;
            }
        }
      }
  }

  openChangeCategoryModal(): void  {
      const dialogRef = this.dialog.open(ChangePreferredCategoryModalComponent, {
          disableClose: false,
          data: this.other_catalogues,
          panelClass: 'change-category-modal'
      });
       this.dialog_subscription = dialogRef.afterClosed().subscribe(result => {
          if (result.action === 'updated') {
              this.other_catalogues = this.helper_service.removedPreferredCatOnCatList(this.all_catalogues);
              // get new preferred category
              this.getPreferredCategory();
          }
      });
  }

  goToScreen(direction): void  {
      if (direction === 'next') {
        this.storage_service.setSessionStorageObject(STORAGE_VARIABLES.OTHER_CATEGORIES, this.selected_catalogues);
          this._route.navigate(['/groups'], { preserveQueryParams: false });
      } else {
          this._route.navigate(['/settings'], { preserveQueryParams: false });
      }
  }
}
