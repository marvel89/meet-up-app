import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import * as _ from 'underscore';
import { ICategories } from '../../shared/interfaces/category.interface';
import { CategoryModel } from '../../shared/model/category.model';
import { API_END_POINTS, DataService} from '../../core/services/data.service';
import { StorageService, STORAGE_VARIABLES } from '../../core/services/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.scss']
})
export class SettingsPageComponent implements OnInit {
  is_loading: boolean = true;
  catalogues: ICategories[];
  selected_cat_id: any;
  selected_cat: CategoryModel;

  constructor(private data_service: DataService,
              private storage_service: StorageService,
              private _route: Router,
              private snackBar: MatSnackBar) {
      const preferred_category: ICategories = this.storage_service.getLocalStorageObject(STORAGE_VARIABLES.PREFERRED_CATEGORY);
      if (preferred_category) { this.selected_cat_id = preferred_category.id; }
      this.selected_cat = new CategoryModel();
  }

  ngOnInit() {
      this.fetchCategories();
  }

  fetchCategories(): void  {
      this.data_service.dataGet(API_END_POINTS.GET_CATEGORIES)
      .subscribe((response: any) => {
        this.is_loading = false;
        if (response) { this.catalogues = response.results; }
      },
      (err: any) => {
          console.log(err);
          this.snackBar.open('An error happened while connecting to the api', 'Ok', { duration: 5000 });
      });
  }

  GoToNextScreen(): void  {
      if (this.selected_cat_id) {
          const selected_cat = _.findWhere(this.catalogues, { id : this.selected_cat_id });
          this.selected_cat = selected_cat ? selected_cat : this.selected_cat;
      }
      this.storage_service.setLocalStorageObject(STORAGE_VARIABLES.PREFERRED_CATEGORY, this.selected_cat);
      this._route.navigate(['/other-categories'], { preserveQueryParams: false });
  }
}
