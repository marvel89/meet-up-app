import { Routes } from '@angular/router';
import { SettingsPageComponent } from './components/settings-page/settings-page';
import { OtherCategoriesComponent } from './components/other-categories/other-categories.component';
import { GroupsComponent } from './components/groups/groups.component';
import { ChangePreferredCategoryModalComponent } from './components/modals/change-preferred-category-modal/change-preferred-category-modal.component';
import { ViewGroupDetailsModalComponent } from './components/modals/view-group-details-modal/view-group-details-modal.component';

const routes: Routes = [
  { path: 'settings', component: SettingsPageComponent },
  { path: 'other-categories', component: OtherCategoriesComponent },
  { path: 'groups', component: GroupsComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'settings' },
];

export const appRouting: any = {
  routes: routes,
  components: [
      SettingsPageComponent,
      OtherCategoriesComponent,
      GroupsComponent,
      ChangePreferredCategoryModalComponent,
      ViewGroupDetailsModalComponent
  ],
  entryComponents: [
      ChangePreferredCategoryModalComponent,
      ViewGroupDetailsModalComponent
  ]
};
